### Installation


**If you use docker**
 
- Run `docker-compose up -d`
- Check your site on http://locahost:3000/

**If you don't have docker**

- make sure you use correct version of node ( nvm use)  
- `yarn`
- `yarn start` 


### Development
 
This project was built with Create React App, please review 
[Create React App](https://github.com/facebook/create-react-app) docs 
for more details. Most used commands. 
  

```sh
# start application http://localhost:3000/
yarn start 

# test 
yarn test

# performs eslint check
yarn eslint

# make production build 
yarn build
```

