FROM node:10.15.3-jessie

WORKDIR /app
COPY package.json .
COPY yarn.lock .

RUN yarn

COPY . /app
