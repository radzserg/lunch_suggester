import di from "../../di";

export const MENU_LOADED = "MENU_LOADED";
export const MENU_LOADING = "MENU_LOADING";
export const MENU_LOAD_ERROR = "MENU_LOAD_ERROR";

/* @type {Chef} */
const chef = di.get("service.chef");

export function openMenu() {
  return dispatch => {
    dispatch({ type: MENU_LOADING });
    (async () => {
      try {
        const recipes = await chef.suggestMenu();
        dispatch({ type: MENU_LOADED, recipes });
      } catch (error) {
        dispatch({ type: MENU_LOAD_ERROR });
      }
    })();
  };
}
