import {
  MENU_LOAD_ERROR,
  MENU_LOADED,
  MENU_LOADING
} from "../actions/menuActions";

const initialState = {
  lading: false,
  error: false,
  recipes: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case MENU_LOADING:
      return {
        ...state,
        loading: true
      };
    case MENU_LOADED:
      return {
        ...state,
        loading: false,
        recipes: action.recipes
      };
    case MENU_LOAD_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    default:
      return state;
  }
}
