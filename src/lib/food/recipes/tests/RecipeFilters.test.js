import Product from "../../products/Product";
import Recipe from "../Recipe";
import { filterByProducts } from "../RecipeFilters";

test("it filter recipes by provided products", () => {
  const anyDate = new Date();
  const products = ["Ham", "Cheese", "Bread", "Butter"].map(
    title => new Product(title, anyDate, anyDate)
  );
  const recipes = [
    new Recipe("Ham and Cheese", ["Ham", "Cheese"]),
    new Recipe("Ham and Cheese and Bacon", ["Ham", "Cheese", "Bacon"]),
    new Recipe("Bread", ["Bread"])
  ];

  const expectedRecipes = [
    new Recipe("Ham and Cheese", ["Ham", "Cheese"]),
    new Recipe("Bread", ["Bread"])
  ];

  const filteredRecipes = recipes.filter(recipe =>
    filterByProducts(recipe, products)
  );
  expect(filteredRecipes).toEqual(expectedRecipes);
});
