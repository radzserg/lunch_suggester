import RecipesBook from "../RecipesBook";
import Recipe from "../Recipe";

test("it returns recipes when valid url provided", async () => {
  const validUrl = "http://www.mocky.io/v2/5c85f7a1340000e50f89bd6c";
  const recipesBook = new RecipesBook(validUrl);
  const recipes = await recipesBook.getRecipes();

  expect(recipes.length).toEqual(5);
  recipes.map(recipe => expect(recipe).toBeInstanceOf(Recipe));
});

test("it throws an error when bad url provided", async () => {
  const invalidUrl = "https://www.mocky.io/v2/invalid-path";
  const recipeBook = new RecipesBook(invalidUrl);
  try {
    await recipeBook.getRecipes();
  } catch (e) {
    expect(e.message).toMatch("Cannot get recipes");
  }
});
