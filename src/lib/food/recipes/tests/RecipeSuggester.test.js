import Product from "../../products/Product";
import Recipe from "../Recipe";
import { orderByProductsBestBefore } from "../RecipeSuggester";

test("it should order according to products best-before date", () => {
  const anyDate = new Date();
  const products = [
    new Product("Ham", new Date("2019-04-05"), anyDate),
    new Product("Cheese", new Date("2019-04-06"), anyDate),
    new Product("Bread", new Date("2019-04-07"), anyDate),
    new Product("Bacon", new Date("2019-04-08"), anyDate)
  ];
  const recipes = [
    new Recipe("Ham and Cheese", ["Ham", "Cheese"]),
    new Recipe("Ham and Cheese and Bacon", ["Ham", "Cheese", "Bacon"]),
    new Recipe("Bread", ["Bread"])
  ];

  const expectedRecipes = [
    new Recipe("Bread", ["Bread"]),
    new Recipe("Ham and Cheese", ["Ham", "Cheese"]),
    new Recipe("Ham and Cheese and Bacon", ["Ham", "Cheese", "Bacon"])
  ];

  const filteredRecipes = orderByProductsBestBefore(recipes, products);
  expect(filteredRecipes).toEqual(expectedRecipes);
});
