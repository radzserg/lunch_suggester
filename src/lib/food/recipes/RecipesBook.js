import fetch from "node-fetch";
import Recipe from "./Recipe";

/**
 * Repository for recipes
 */
class RecipesBook {
  constructor(providerUrl, logger) {
    this.providerUrl = providerUrl;
  }

  /**
   * Return recipes list
   * @returns {Recipe[]}
   */
  async getRecipes() {
    const response = await fetch(this.providerUrl);
    const recipesData = await RecipesBook.parseResponse(response);

    return recipesData.map(
      recipeData => new Recipe(recipeData.title, recipeData.ingredients)
    );
  }

  /**
   * Validate response
   * @param response
   * @returns {[]}
   */
  static async parseResponse(response) {
    if (response.status >= 200 && response.status < 300) {
      const products = await response.json();
      return products.recipes;
    }
    const error = response.statusText;
    throw new Error(`Cannot get recipes: ${error}`);
  }
}

export default RecipesBook;
