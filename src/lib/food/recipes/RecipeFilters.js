/**
 * Filter recipes - based on provided product list.
 * @param {Recipe} recipe
 * @param {Product[]} products
 */
function filterByProducts(recipe, products) {
  const productNames = products.map(product =>
    product.title.toLowerCase().trim()
  );
  const missingProducts = recipe.ingredients.filter(recipeProduct => {
    return !productNames.includes(recipeProduct.toLowerCase().trim());
  });
  return missingProducts.length === 0;
}

export { filterByProducts };
