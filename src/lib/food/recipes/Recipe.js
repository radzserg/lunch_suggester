class Recipe {
  /**
   *
   * @param {string} title
   * @param {string[]} ingredients
   */
  constructor(title, ingredients) {
    this.title = title;
    this.ingredients = ingredients;
  }
}

export default Recipe;
