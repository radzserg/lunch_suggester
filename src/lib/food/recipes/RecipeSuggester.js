/**
 * Finds oldest product in the list
 * @param {Product[]} products
 * @param {Product} products
 */
function findOldestBestBeforeDate(products) {
  if (!products.length) {
    return null;
  }
  return products.reduce((oldest, product) => {
    return product.bestBefore < oldest ? product.bestBefore : oldest;
  }, products[0].bestBefore);
}

/**
 * Return products for provided recipe ingredients
 * @param {Recipe} recipe
 * @param {Product[]} products
 * @returns {Product[]}
 */
function recipeProducts(recipe, products) {
  return recipe.ingredients.map(requiredProduct =>
    products.find(product => product.title === requiredProduct)
  );
}

/**
 * Order recipes by product best-before date.
 * Recipes containing products with oldest best-before
 * should be sorted to the bottom of the list
 * @param {Recipe[]} recipes
 * @param {Product[]} products
 */
function orderByProductsBestBefore(recipes, products) {
  return recipes.sort((recipeA, recipeB) => {
    const recipeAProducts = recipeProducts(recipeA, products);
    const recipeBProducts = recipeProducts(recipeB, products);

    const recipeAOldestBestBefore = findOldestBestBeforeDate(recipeAProducts);
    const recipeBOldestBestBefore = findOldestBestBeforeDate(recipeBProducts);
    return recipeAOldestBestBefore < recipeBOldestBestBefore;
  });
}

export { orderByProductsBestBefore };
