import { useByDateFilter } from "./products/ProductFilters";
import { filterByProducts } from "./recipes/RecipeFilters";
import { orderByProductsBestBefore } from "./recipes/RecipeSuggester";

class Chef {
  /**
   * @param {RecipesBook} recipeBook
   * @param {Fridge} fridge
   */
  constructor(recipeBook, fridge) {
    this.recipeBook = recipeBook;
    this.fridge = fridge;
  }

  /**
   * Suggest menu based on the product that we have
   * and available menu.
   *
   * @return {Recipe[]}
   */
  async suggestMenu() {
    const products = await this.fridge.getProducts();
    const availableProducts = products.filter(useByDateFilter);

    const recipes = await this.recipeBook.getRecipes();
    const availableRecipes = recipes.filter(recipe =>
      filterByProducts(recipe, availableProducts)
    );

    return orderByProductsBestBefore(availableRecipes, products);
  }
}

export default Chef;
