import Chef from "../Chef";
import Product from "../products/Product";
import Recipe from "../recipes/Recipe";

test("chef suggest menu based on the product and menu", async () => {
  const mockFridge = {
    async getProducts() {
      const tomorrow = new Date();
      tomorrow.setDate(new Date().getDate() + 1);
      return [
        new Product("Ham", tomorrow, tomorrow),
        new Product("Mustard", tomorrow, tomorrow),
        new Product("Mushrooms", tomorrow, tomorrow)
      ];
    }
  };
  const mockRecipeBook = {
    async getRecipes() {
      return [
        new Recipe("Ham and Mustard", ["Ham", "Mustard"]),
        new Recipe("Mushrooms", ["Mushrooms"]),
        new Recipe("Tomato", ["Tomato"])
      ];
    }
  };

  const chef = new Chef(mockRecipeBook, mockFridge);
  const menu = await chef.suggestMenu();

  const expectedRecipes = [
    new Recipe("Ham and Mustard", ["Ham", "Mustard"]),
    new Recipe("Mushrooms", ["Mushrooms"])
  ];
  expect(menu).toEqual(expectedRecipes);
});
