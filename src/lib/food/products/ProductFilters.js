/**
 * Filter products with use-by date in the past
 *
 * @param {Product} product
 * @returns {boolean}
 */
function useByDateFilter(product) {
  const now = new Date();
  return product.useBy > now;
}

export { useByDateFilter };
