import Product from "../Product";
import { useByDateFilter } from "../ProductFilters";

test("it filter products with use by date in the past", () => {
  const anyDate = new Date();
  const tomorrow = new Date();
  tomorrow.setDate(anyDate.getDate() + 1);

  const products = [
    new Product("Ham", anyDate, new Date("2019-04-05")),
    new Product("Mustard", anyDate, tomorrow),
    new Product("Tomato", anyDate, new Date()),
    new Product("Mushrooms", anyDate, tomorrow)
  ];

  const filteredProducts = products.filter(useByDateFilter);
  const expectedResult = [
    new Product("Mustard", anyDate, tomorrow),
    new Product("Mushrooms", anyDate, tomorrow)
  ];

  expect(filteredProducts).toEqual(expectedResult);
});
