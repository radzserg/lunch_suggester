import Fridge from "../../../../lib/food/products/Fridge";
import Product from "../../../../lib/food/products/Product";

test("it returns list of the products when valid url provided", async () => {
  const validUrl = "https://www.mocky.io/v2/5cac82f1300000664f10368f";
  const fridge = new Fridge(validUrl);
  const products = await fridge.getProducts();

  expect(products.length).toEqual(16);
  products.map(product => expect(product).toBeInstanceOf(Product));
});

test("it throws an error when bad url provided", async () => {
  const invalidUrl = "https://www.mocky.io/v2/invalid-path";
  const fridge = new Fridge(invalidUrl);
  try {
    await fridge.getProducts();
  } catch (e) {
    expect(e.message).toMatch("Cannot get products from the fridge");
  }
});
