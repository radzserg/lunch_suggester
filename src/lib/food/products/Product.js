/**
 * Represents product
 */
class Product {
  /**
   * @param {string} title
   * @param {Date} bestBefore
   * @param {Date} useBy
   */
  constructor(title, bestBefore, useBy) {
    this.title = title;
    this.bestBefore = bestBefore;
    this.useBy = useBy;
  }
}

export default Product;
