import fetch from "node-fetch";
import Product from "./Product";

/**
 * Fridge keeps product and knows there state
 */
class Fridge {
  /**
   * @param providerUrl - URL to the API provider
   */
  constructor(providerUrl) {
    this.providerUrl = providerUrl;
  }

  /**
   * Return a list of the products in the fridge
   * @returns {Promise<Product[]>}
   */
  async getProducts() {
    const response = await fetch(this.providerUrl);
    const productsData = await Fridge.parseResponse(response);
    return productsData.map(
      productData =>
        new Product(
          productData.title,
          new Date(productData["best-before"]),
          new Date(productData["use-by"])
        )
    );
  }

  /**
   * Parse response from provider. Makes sure
   * that response is valid. Otherwise log an error
   * and return empty list
   * @param response
   * @returns {Promise<Array|*>}
   */
  static async parseResponse(response) {
    if (response.status >= 200 && response.status < 300) {
      const products = await response.json();
      return products.ingredients;
    }
    const error = response.statusText;
    throw new Error(`Cannot get products from the fridge: ${error}`);
  }
}

export default Fridge;
