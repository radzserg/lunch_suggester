/**
 * Stub for DIC,
 * @todo work on solid solution like http://inversify.io/ or similar
 */
class DependencyInjectionContainer {
  constructor() {
    this.dependencies = {};
  }

  register(key, value) {
    this.dependencies[key] = value;
    return this;
  }

  get(key) {
    if (!this.dependencies[key]) {
      throw new Error(`Missing dependency: ${key}`);
    }
    return this.dependencies[key];
  }
}

export default DependencyInjectionContainer;
