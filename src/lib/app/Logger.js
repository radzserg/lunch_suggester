/**
 * Stub for application logger.
 *
 * Will log to console. In a real world we may want
 * to have more complex logic here.
 */
class Logger {
  error() {
    console.error("Lunch Logger", arguments);
  }
}

export default Logger;
