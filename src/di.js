import DependencyInjectionContainer from "./lib/app/DependencyInjectionContainer";
import RecipesBook from "./lib/food/recipes/RecipesBook";
import Fridge from "./lib/food/products/Fridge";
import Chef from "./lib/food/Chef";
import Logger from "./lib/app/Logger";

// * @todo move config vars to config files

const di = new DependencyInjectionContainer();
di.register(
  "params.recipes.providerUrl",
  "http://www.mocky.io/v2/5c85f7a1340000e50f89bd6c"
)
  .register(
    "params.fridge.providerUrl",
    "https://www.mocky.io/v2/5cac82f1300000664f10368f"
  )
  .register(
    "service.recipeBook",
    new RecipesBook(di.get("params.recipes.providerUrl"))
  )
  .register("service.fridge", new Fridge(di.get("params.fridge.providerUrl")))
  .register(
    "service.chef",
    new Chef(di.get("service.recipeBook"), di.get("service.fridge"))
  )
  .register("service.logger", new Logger());

export default di;
