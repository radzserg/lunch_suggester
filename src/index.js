import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-grid.css";
import "bootstrap/dist/css/bootstrap-reboot.css";

import React from "react";
import ReactDOM from "react-dom";

import * as serviceWorker from "./serviceWorker";
import di from "./di";
import App from "./components/App/App";
import { Provider } from "react-redux";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import configureStore from "./store/configureStore";

const store = configureStore();
const logger = di.get("service.logger");

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary logger={logger}>
      <App />
    </ErrorBoundary>
  </Provider>,
  rootElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
