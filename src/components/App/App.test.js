import React from "react";
import { shallow } from "enzyme";

import App from "./App";
import LunchMenu from "../LunchMenu/LunchMenu";

it("renders without crashing", () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find(LunchMenu).length).toEqual(1);
});
