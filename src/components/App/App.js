import React from "react";
import { Container } from "react-bootstrap";

import LunchMenu from "../LunchMenu/LunchMenu";

const App = () => (
  <div className="App">
    <main role="main" className="flex-shrink-0">
      <Container className="mt-5">
        <LunchMenu />
      </Container>
    </main>
  </div>
);

export default App;
