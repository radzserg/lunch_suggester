import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";

import { openMenu } from "../../store/actions/menuActions";
import LunchMenuView from "./LunchMenuView";

/**
 * Lunch menu with connected to state
 */
class LunchMenu extends LunchMenuView {}

LunchMenu.propTypes = {
  dispatch: PropTypes.func,
  menu: PropTypes.shape({
    recipes: PropTypes.array,
    loading: PropTypes.bool,
    error: PropTypes.bool
  })
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ openMenu }, dispatch);

const mapStateToProps = state => {
  const { recipes, loading, error } = state.menu;
  return {
    recipes,
    loading,
    error
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LunchMenu);
