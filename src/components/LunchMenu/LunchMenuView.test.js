import { shallow } from "enzyme";
import React from "react";
import { Button, Spinner, Alert } from "react-bootstrap";

import LunchMenuView from "./LunchMenuView";
import RecipeList from "./RecipeList/RecipeList";

it("renders default state", () => {
  const wrapper = shallow(
    <LunchMenuView recipes={undefined} loading={false} error={false} />
  );
  const button = wrapper.find(Button);

  expect(button.text()).toEqual("What's for a lunch");
  expect(button.length).toEqual(1);
  expect(wrapper.find(Spinner).length).toEqual(0);
  expect(wrapper.find(Alert).length).toEqual(0);
  expect(wrapper.find(RecipeList).length).toEqual(0);
});

it("shows spinner and hide button when loading", () => {
  const wrapper = shallow(<LunchMenuView recipes={undefined} loading={true} />);
  expect(wrapper.find(Button).length).toEqual(0);
  expect(wrapper.find(Spinner).length).toEqual(1);
});

it("shows recipes and hide button when recipes provided", () => {
  const wrapper = shallow(<LunchMenuView recipes={[]} loading={true} />);
  expect(wrapper.find(Button).length).toEqual(0);
  expect(wrapper.find(RecipeList).length).toEqual(1);
});

it("shows error when error provided", () => {
  const wrapper = shallow(<LunchMenuView error={true} />);
  const alert = wrapper.find(Alert);
  expect(alert.length).toEqual(1);
  expect(alert.text()).toEqual("Oops, something went wrong. No lunch today.");
});
