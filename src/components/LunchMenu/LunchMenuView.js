import React from "react";
import PropTypes from "prop-types";
import { Alert, Button, Spinner } from "react-bootstrap";

import RecipeList from "./RecipeList/RecipeList";
import Recipe from "../../lib/food/recipes/Recipe";

class LunchMenuView extends React.Component {
  render() {
    const { recipes, loading, error, openMenu } = this.props;
    const showButton = !recipes && !loading;

    return (
      <div className="Chef">
        {showButton && (
          <Button type="button" onClick={openMenu}>
            What&apos;s for a lunch
          </Button>
        )}

        {loading && (
          <Spinner animation="border" role="status">
            <span className="sr-only">
              Let's see what we have in the fridge
            </span>
          </Spinner>
        )}
        {error && (
          <Alert variant="danger">
            Oops, something went wrong. No lunch today.
          </Alert>
        )}
        {recipes && <RecipeList recipes={recipes} />}
      </div>
    );
  }
}

LunchMenuView.propTypes = {
  openMenu: PropTypes.func,
  recipes: PropTypes.arrayOf(PropTypes.instanceOf(Recipe)),
  loading: PropTypes.bool,
  error: PropTypes.bool
};

export default LunchMenuView;
