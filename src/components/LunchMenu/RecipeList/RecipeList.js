import React, { Component } from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import PropTypes from "prop-types";
import { Alert } from "react-bootstrap";

import "./RecipeList.css";

import Recipe from "../../../lib/food/recipes/Recipe";
import RecipeItem from "./RecipeItem/RecipeItem";

class RecipeMenu extends Component {
  render() {
    const { recipes } = this.props;

    if (!recipes.length) {
      return (
        <Alert variant="info">
          No recipes today, looks like your fridge is empty.
        </Alert>
      );
    }

    const items = recipes.map(recipe => (
      <RecipeItem key={recipe.title} recipe={recipe} />
    ));

    return (
      <div>
        <ReactCSSTransitionGroup
          transitionName="recipe-list"
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionEnter={false}
          transitionLeave={false}
        >
          <div>{items}</div>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

RecipeMenu.propTypes = {
  recipes: PropTypes.arrayOf(PropTypes.instanceOf(Recipe))
};

export default RecipeMenu;
