import React, { Component } from "react";
import PropType from "prop-types";
import { Card } from "react-bootstrap";

import Recipe from "../../../../lib/food/recipes/Recipe";

class RecipeItem extends Component {
  /**
   * Render ingredient
   * @param {Recipe} recipe
   */
  static renderIngredients(recipe) {
    return recipe.ingredients.map(ingredient => (
      <div key={ingredient}>{ingredient}</div>
    ));
  }

  render() {
    const { recipe } = this.props;

    return (
      <Card className="mb-5">
        <Card.Body>
          <Card.Title>{recipe.title}</Card.Title>
          <Card.Subtitle>What you'll need</Card.Subtitle>
          <div className="ingredients">
            {RecipeItem.renderIngredients(recipe)}
          </div>
        </Card.Body>
      </Card>
    );
  }
}

RecipeItem.propTypes = {
  recipe: PropType.instanceOf(Recipe)
};

export default RecipeItem;
