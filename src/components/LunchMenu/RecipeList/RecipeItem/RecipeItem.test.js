import { shallow } from "enzyme";
import React from "react";
import { Card } from "react-bootstrap";

import RecipeItem from "./RecipeItem";
import Recipe from "../../../../lib/food/recipes/Recipe";

it("renders recipe item", () => {
  const recipe = new Recipe("Ham", ["cheese", "ham"]);

  const wrapper = shallow(<RecipeItem recipe={recipe} />);

  expect(wrapper.find(Card.Title).text()).toEqual("Ham");

  const ingredients = wrapper.find(".ingredients");
  expect(ingredients.children().length).toEqual(2);
  expect(ingredients.childAt(0).text()).toEqual("cheese");
  expect(ingredients.childAt(1).text()).toEqual("ham");
});
