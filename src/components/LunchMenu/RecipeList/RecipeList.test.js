import { shallow } from "enzyme";
import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import { Alert } from "react-bootstrap";

import Recipe from "../../../lib/food/recipes/Recipe";
import RecipeItem from "./RecipeItem/RecipeItem";
import RecipeList from "./RecipeList";

it("renders recipes", () => {
  const recipes = [new Recipe("Ham", []), new Recipe("Cheese", [])];
  const wrapper = shallow(<RecipeList recipes={recipes} />);

  expect(wrapper.find(ReactCSSTransitionGroup).length).toEqual(1);
  expect(wrapper.find(RecipeItem).length).toEqual(2);
  expect(wrapper.find(Alert).length).toEqual(0);
});

it("shows message about empty list", () => {
  const wrapper = shallow(<RecipeList recipes={[]} />);

  expect(wrapper.find(ReactCSSTransitionGroup).length).toEqual(0);
  expect(wrapper.find(RecipeItem).length).toEqual(0);

  const alert = wrapper.find(Alert);
  expect(alert.length).toEqual(1);
  expect(alert.text()).toEqual(
    "No recipes today, looks like your fridge is empty."
  );
});
